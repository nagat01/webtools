﻿module MenuUI
open FStap open __ open System open System.Windows open System.Windows.Controls

let sp        = HStackPanel() 
let  btnSpeak = ShadowedButton(Ico.download, "ddf")            $ sp
let  btnStop  = ShadowedButton(Ico.pause,    "dfd", Ico.start) $ sp
let  btnAdd   = ShadowedButton(Ico.add,      "dff")            $ sp
let   _       = UrlListUI.spBtns$ sp


btnSpeak =+! { 
  let! _, ss = SpeechUI.browserTopText
  tabIndex <- -1
  RtbUI.speakStrings ss }

btnStop =+{
  if btnStop.v 
  then RtbUI.pause
  else RtbUI.restart }

btnAdd =+! { 
  do! UrlListUI.add }
