﻿[<AutoOpen>]
module __.Misc
open FStap open System open System.Windows open System.Windows.Controls
open System.Threading


let Ico = ImageProvider 24.

let [<Literal>] csv = "url, xpath"

let wnd = Window' "AltSpeak"

let pageRead = Event<u>()
let mutable tabIndex = -1

// func
let immediateRestart (src:CancellationTokenSource ref) =
  src.Value.Cancel()
  src := new CancellationTokenSource()
  AsyncStartBuilder((fun a -> Async.StartImmediate(a, src.Value.Token)), (fun f -> f()))