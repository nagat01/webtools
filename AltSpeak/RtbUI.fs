﻿module RtbUI
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Threading
open System.Windows.Documents

let source = ref (new CancellationTokenSource())
let mutable prevIl = None'
let mutable curIl  = None'
let mutable prevText = ""

let rtb     = RichTextBox() $ vscrAuto 
let  blocks = rtb.Document.Blocks $ fun __ -> __.Clear() 
let   para  = Paragraph() $ blocks.Add
let    ils  = para.Inlines


// func
let setStrings ss =
  ils.Clear()
  for s in ss do
    ils <+ Run (s + "\r\n")

let speakIl (il:Inline) =
  Speech.stop 
  immediateRestart source {
  curIl <*- il
  while curIl.ok do
    match curIl.v with
    | :? Run as run ->
      run |> "aaf".bg
      if rtb.IsMouseOver.n then
        scrollToText run rtb
      prevText <- run.Text
      let! completed = 
        Speech.speak prevText >=< 
        rtb.pmdown -%% false
      run |> (completed.If "afa" "faa").bg
      if completed then
        let mutable i = 0
        let next = ref run.NextInline
        let isNext() = 
          (tryCast<Run> (!next)).is(fun __ -> __.Text = prevText)
        while &i +=! 1 < 10 && isNext() do
          next := (!next).NextInline
        curIl <*- !next
    | _ -> 
      curIl.none 
  pageRead <*- () }

let speakStrings (ss:_ seq) =
  if ss.ok then
    setStrings ss
    speakIl ils.head 

let restart<'__> =
  prevIl |%| speakIl

let pause<'__> =
  curIl |%| fun __ ->
    Speech.stop
    source.Value.Cancel()
    prevIl <*- __


// events
rtb.pmdown => fun e ->
  tryCast<Run> e.OriginalSource |%| speakIl
