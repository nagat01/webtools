﻿[<AutoOpen>]
module __.SettingUI
open FStap open __ open System.Windows.Controls


let dp                = DockPanel()
let  ignoredChars     = PTbx("除外文字", "▼► ()*+-/%") $ dp.top
let  ignoreStrings    = PTbxLong("除外文字列", "") $ dp.top
let  shiftJISPatterns = PTbx("ShiftJISのページ", "") $ dp.top
let  xpath            = PTbxLong("xpath", csv) $ dp.top

