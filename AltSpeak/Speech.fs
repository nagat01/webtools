﻿[<AutoOpen>]
module __.Speech
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Collections.Generic
open System.Globalization
open System.Speech.Synthesis


// vars
let english  = Speech' "en-US"
let japanese = Speech' "ja-JP"

let queue = Queue<s>()


// func
let enq s   = queue.enq s
let enqs ss = ss |> Seq.iter enq
let inline enqf fmt = kf queue.enq fmt

let clear<'__> = queue.Clear()

let volume volume =
  english .volume <- volume
  japanese.volume <- volume

let speed speed =
  english .speed <- speed
  japanese.speed <- speed

let speak (s:s) =
  let speech = s.isEnglish.If english japanese 
  let prompt = speech.speak s 
  speech.speech.SpeakCompleted
  >%? fun __ -> __.Prompt = prompt
  |%> fun __ -> __.Cancelled.n

let stop<'__> =
  english .speech.SpeakAsyncCancelAll()
  japanese.speech.SpeakAsyncCancelAll()