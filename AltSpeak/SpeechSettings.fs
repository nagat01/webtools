﻿[<AutoOpen>]
module __.SpeechSettings
open FStap open __ open System open System.Windows open System.Windows.Controls


// settings
let _wait     = pshf "読み上げの間隔" 0.
let _capacity = pshi "貯めこむ文章の最大数" 100
let _replace  = pshs "単語の置き換え" "＠,あっと;@,あっと;www,;ｗｗｗ,;WWW,;WWW,"
let _remove   = pshs "読み上げない文字" "()（）･´｀ω+ﾉ!:/・"


// funcs
let educateTts (s:s) =
  let replace = 
    try _replace.v.kvs 
    with _ -> msg "読み上げる文字列を変換するためのjsonが間違っているようです。"; [||]
  let remove  = _remove.v
  s.words.map(fun w -> w.urlOk.If "ゆーあーるえる省略" w).concat(" ") 
    .repMany(replace).filter(remove.have >> not) |> String.Concat


let ini = educateTts.ig

