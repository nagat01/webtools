﻿module SpeechUI
open FStap open __
open System.Speech.Synthesis


// ui
let sp           = HStackPanel()
let  browser     = BrowserSelector() $ sp
let  cmbEnglish  = speechComboBoxEnglish  $ sp $ mgnw 1
let  cmbJapanese = speechComboBoxJapanese $ sp $ mgnw 1
let  sliVolume   = TitledSlider("音量",   "vol",    0.,   100., 25.) $ sp $ mgnw 1
let  sliSpeed    = TitledSlider("速度",   "speed",  -10., 10.,  0. ) $ sp $ mgnw 1
let  sliRepeat   = TitledSlider("繰返し", "repeat", 1.,   1.,   5. ) $ sp $ mgnw 1


// funcs
let speak (text:s) =
  Speech.speak text |> ig      

let browserTopUrl<'__> = 
  browser.topUrl ""

let browserTopText<'__> =
  match browser.topUrl "" with
  | Some url -> getSentencesUrl url
  | _        -> async { return ("", []) }


let changed = sliSpeed >< sliVolume >< cmbJapanese >< cmbEnglish


// event
sliVolume =+ { Speech.volume sliVolume.v.ri }
sliSpeed  =+ { Speech.speed  sliSpeed.v.ri }


// init
let ini = sp.ig
