﻿[<AutoOpen>]
module __.TextExtractor
open FStap open __ open System
open FSharp.Data
open System.Text
open System.IO


// constants
let CandidateXPaths = [
  "h2;h3;h4;p",    1.
  "h2;h3;h4;span", 1.
  "h2;h3;h4;div",   0.1 
  ]

let Replacers<'__> = [
  @"https?://\S+",         ""
  @"<a.*?>.*?http.*?</a>", ""
  @"(\r\n|\r|\n|/|<.*?>)", " "          
  @"(F|C)#",               "$1 sharp"
  @"\.NET",                "dot NET"
  @"([a-z])([A-Z])",       "$1 $2" 
  ]

let IgnoredChars<'__>   = [ for c in ignoredChars.v -> c.s ]
let IgnoredStrings<'__> = [ for s in ignoreStrings.ss -> s, "" ]

let NotReadChars<'__> = [ 
  for c in " :" -> c.s 
  for d in 0..9 -> d.s ]


// funcs
let getHtmlJapanese (url:s) = task' {
  let sjis = Encoding.GetEncoding "Shift_JIS"
  use client = new Net.WebClient()
  let bs     = client.DownloadData url
  let html   = sjis.GetString bs
  let html   = HtmlDocument.Parse html
  yield html }

let formatText (text:s) =
  text.trim
    .repMany(IgnoredStrings)
    .repMany(Replacers)
    .htmlDecode
    .remMany(IgnoredChars)

let readLength (text:s) =
  text
    .htmlDecode
    .remMany(NotReadChars)
    .remMany(IgnoredChars)
    .len


let extractNodes (html:HtmlDocument) (queries:s) = 
  let funcs = [
    for query in queries.trim.split ";" do
      let query = query.trim.split " "
      let name = query.[0]
      if query.len > 1 then
        let ic   = query.[1]
        yield fun (node:HtmlNode) -> node.Name() = name && (node.HasAttribute("id", ic) || node.HasAttribute("class", ic))
      else
        yield fun (node:HtmlNode) -> node.Name() = name ]
  html.Body().Descendants(fun node -> funcs.any(fun f -> f node))


// get sentence
let mutable i = 0
let getSentencesXPath (html:HtmlDocument) xpath = 

  let skippedNode (node:HtmlNode) =
    [""; "h2"; "h3"; "h4"].have (node.Name()) ||
    readLength (node.InnerText()) < 5

  let skip (__:_ seq) = __.rev |> Seq.skipWhile skippedNode

  let extract (node:HtmlNode) = 
    node.InnerText() |> formatText

  extractNodes html xpath |> skip |> skip |%> extract

let getSentenceAndLength xpath weight html =
  let ss = getSentencesXPath html xpath 
  let len = ss.sumBy String.length *. weight
  ss, len

let getHtml (url:s) = async {
  let isSJisPage = shiftJISPatterns.ss.exists url.Contains
  return! isSJisPage.If getHtmlJapanese HtmlDocument.AsyncLoad url }

let getSentences csv (url:s) = async {
  let! html = getHtml url
  let xpaths = 
    match getXPath csv url with 
    | Some xpath -> [xpath, f.MaxValue] 
    | _          -> CandidateXPaths
  let ss = 
    seq { 
    for xpath, weight in xpaths ->
      getSentenceAndLength xpath weight html }
    |> Seq.maxBy _x |> x_ 
    |> Seq.toList
  let html = getHtmlTitle (html.Body().InnerText())
  return html, ss }

let getSentencesUrl url =
  getSentences xpath.v url