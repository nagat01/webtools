﻿module UrlListUI
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.IO

(pwd +/ "cache").mkDir

let path (url:s) =
  pwd +/ "cache" +/ url.remMany "\/.:=?".ss

let idxList = pshi "idxList" 0

// ui
type UrlPanel(idx:i, pa:ListBox, title:s, url:s, ss:s seq) as __ =
  inherit DockPanel()
  let btnGet    = Button'("再取得", "efe", "dfd", "fff") $ __ $ padw 2
  let btnRemove = Button'("削除",   "fee", "fdd", "fff") $ __ $ padw 2
  let lblTitle  = Label' title        $ __ $ padw 2
  let lblUrl    = Label'("["+url+"]") $ __ $ padw 2

  do
    lblTitle.lclick >< lblUrl.lclick =+ { __.speak }
    btnGet =+! {
      let! title,ss = getSentencesUrl url
      __.title <- title
      __.ss    <- seq ss
      lblTitle <*- title }
    btnRemove =+ { pa.rem __ }

  member val title = title with get, set
  member val url   = url   with get, set
  member val ss    = ss    with get, set
  
  member __.speak = 
    tabIndex <- idx
    RtbUI.speakStrings __.ss


type UrlList(idx:i) as __ =
  inherit ListBox()
  let kTitle = sf "title%d" idx
  let kUrl = sf "urls%d" idx

  do
    // events
    pageRead =+ {
      if idx = tabIndex && __.idx < __.is.ilast then
        __.idx <- __.idx + 1
        (__.item :?> UrlPanel).speak }

    loaded =+ {
      for title, url in Seq.zip (pref.ss kTitle) (pref.ss kUrl) do
        let ss = path url |> File.ReadLines 
        UrlPanel (idx, __, title, url, ss) |>* __ }

    closing =+ {
      let ups = extract __.is : UrlPanel seq
      pref.Ss kTitle [ for up in ups -> up.title ]
      pref.Ss kUrl   [ for up in ups -> up.url   ]
      for up in ups do 
        path(up.url).writeLines up.ss
      }

  member __.add = async {
    let! url = SpeechUI.browserTopUrl
    let! title, ss = getSentencesUrl url
    UrlPanel(idx, __, title, url, ss) |>* __ }


// ui
let urlLists = [| for i in 0..9 -> UrlList(i) |]
let lbl = Label' urlLists.[idxList.v]

let spBtns = HStackPanel()
let btns = [| 
  for i in 0..9 ->
    let btn = Button'(i,"0fff") $ bdr 1 "ccc" $ padw 6 $ h 26 $ fwi 950 $ "777".fg
    let lbl = Label' btn $ spBtns 
    btn =+ { idxList <*- i }
    lbl, btn |]


let add<'__> =
  urlLists.[idxList.v].add

idxList => fun __ ->
  urlLists.[__] |>* lbl
  for i in 0..9 do
    fst btns.[i] |> ((i = __).If "fed" "fff").bg

let ini = lbl.ig