﻿[<AutoOpen>]
module __.XPath
open FStap open __

type XPath = FSharp.Data.CsvProvider<csv>

let getXPath csv (url:s) =
  let xpaths = 
    if "xpath.csv".fileOk.n then "xpath.csv".write csv
    [ for column in XPath.Parse(csv).Rows -> column.Url.trim , column.Xpath.trim ]
  xpaths.find url.Contains

