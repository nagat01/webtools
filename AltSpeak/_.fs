open FStap open __ open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "AltSpeak"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "AltSpeak: ウェブページを読み上げるツール"; STAThread>] SET_ENG_CULTURE


let dp         = DockPanel()          $ wnd $ lastChildFill
let  spTop     = HStackPanel()         $ dp.top
let   _        = SpeechUI.sp     $ spTop
let   btnPref  = Button' btnPref $ spTop
let  _         = MenuUI.sp       $ dp.top
let  lbl       = Label'()        $ dp.bottom $ vaStretch $ vcaStretch
let   dpBottom = DockPanel()          $ lbl
let    _       = UrlListUI.lbl   $ dpBottom.top
let    _       = RtbUI.rtb       $ dpBottom.bottom


btnPref =+ {
  lbl <*- If<o> (lbl =. dpBottom) SettingUI.dp dpBottom }


trace true {

UrlListUI.ini
SpeechUI.ini

INIT_SHARED_VALUES
wnd.run

}

