open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "SoundScaper"; version "0.2"; company "ながと"; copyright "Copyright © ながと 2014";
  title "SoundScaper: __"; STAThread>] SET_ENG_CULTURE

let isPlay = sh false
let me = MediaElement'()
let play () = me.Play (); isPlay <*- true
let pause() = me.Pause(); isPlay <*- false


// menu
let inline withLabel pa ctt = 
  DockPanel() $ lastChildFill $ cs [ctt $ haStretch] $ "fff".bg |>* pa

let dp          = DockPanel () $ cs [me] $ wnd
let hsp         = HStackPanel() $ dp.top
let  spL        = HStackPanel() $ hsp
let  spR        = HStackPanel() $ hsp
let   textPos   = Lbl "Pos" $ "fff".fg $ spR
let   sliPos    = Slider() $ w 80 $ vaCenter $ spR
let   textVol   = Lbl "Vol" $ "fff".fg $ spR
let   sliVol    = Slider'(0., 1., pref.f' "vol" 0.5, 50.) $ psave "vol" $ spR
let   btnDl     = Button' Ico.download $ spR $ tip "Youtubeの動画をダウンロードします。クリップボードにURLを保存して、このボタンを押してください。"
let   chkReDir  = CheckBox() $ v (pref.bOrF "reuseDir") $ psave "reuseDir" $ padw 2 $ vaCenter $ spR
let   tbxFilter = TextBox'() $ spR $ w 100 $ ploads' "filter" "" $ psave "filter"
let   browser   = BrowserSelector() $ withLabel spR
let lbxDl       = ListBox (MaxHeight=162.) $ dp.top
let lbxSong     = ListView() $ hscrHide' $ dp.top

let pnts = pref.mkDic "pnts"
type SongI (file:s) as __ =
  inherit DockPanel()
  let _pnt = sh (pnts.find file.name |%> float |? 0.)
  let btnDel   = Button'(Ico.remove $ h 16) $ __.right
  let sp       = StackPanel()              $ mgnl 2 $ __.right
  let  btnUp   = Button'("▲", bg="fc7") $ "c60".fg $ sp       $ fsz 6 $ padw 5
  let  btnDown = Button'("▼", bg="7cf") $ "06c".fg $ sp       $ fsz 6 $ padw 5 
  let lblPnt   = Label'()              $ mgnw 1 $ __.right
  let lblName  = Label' file.nameNoExt $ mgnw 1 $ __.left
  let lblDir   = Label' file.dir       $ mgnw 1 $ __.right $ "fcc".fg $ haRight
  do 
    _pnt => fun pnt ->
      lblPnt <*- sf "%1.2f" pnt
      let co = match pnt with GT 7.5 -> "fc0" | GT 5. -> "0f0" | GT 2.5 -> "0dd" | GT 0. -> "44f" | GT -5. -> "777" | _ -> "f00"
      co.fg lblPnt
      pnts.[file.name] <- pnt.s
    _pnt.ini
    btnUp -%% 0.5 >=< btnDown -%% -0.5 => ((+) >> __.mpnt)
    btnDel =+ { lbxSong.rem __ }
    lblName.mdown >< lblDir.mdown =+ { __.mpnt ((+) 1.5) }
    lbxSong.sized =+ { __.w <- lbxSong.rw - 24. }
    tbxFilter >< loaded =+ { __.Vis __.vis }
  member __.vis = file.hasLower tbxFilter.Text
  member __.file = file
  member __.pnt = _pnt.v
  member __.Pnt v = _pnt <*- v
  member __.mpnt f = 
    let prev = _pnt.v
    _pnt <*- f _pnt.v
    let d = (_pnt.v - prev) / lbxSong.cnt.f
    for si:SongI in Seq.cast lbxSong.is do 
      si.Pnt (si.pnt - d)

// funcs
let mkSongI file = SongI file
let songIToFile   __ = ((__:o) :?> SongI).file
let isVis         __ = ((__:o) :?> SongI).vis
let IsSongExist<'__> = lbxSong.is.any(fun o -> tryb { V (isVis o) } )
let IsSongOk<'__>    = lbxSong.item.ok && isVis lbxSong.item

let rec playSong _ = 
  if IsSongOk then 
    let item = lbxSong.item
    try
      me <*- Uri(songIToFile lbxSong.item)
      play()
    with _ ->
      next ()
      lbxSong.rem item
  else
    next ()
and playOrPause _ = 
  if me.HasAudio then 
    isPlay.v.If pause play ()
and next(_:obj) = 
  if IsSongExist then
    lbxSong.selNext; while IsSongOk.n do lbxSong.selNext
    playSong ()

let sortSongs _ =
  let si = lbxSong.item
  let ss = (Seq.cast<SongI> lbxSong.is).array
  lbxSong.clear
  ss 
  |> Seq.sortBy(fun si -> Rng.f -4. 4. - si.pnt * Rng.f 0.3 1.)
  |%|* lbxSong
  if si.ok then lbxSong.item <- si

let addFiles (files:s []) =
  files.shuffle
  >%?~ (lbxSong.is |%> songIToFile).have 
  |%> mkSongI
  |%|* lbxSong
  sortSongs()
  try' { lbxSong.item <- lbxSong.is.head }
  playSong() 

let openFiles isDir _ = 
  openFiles "openDir" "mp3 wav flv" isDir addFiles

let back _ = 
  if me.po < msec 500 && lbxSong.idx <> - 1 && IsSongExist then 
    lbxSong.selPrev; while IsSongOk.n do lbxSong.selPrev
  else 
    me.po <- Zero


// buttons
let mkBtn  (img:Image) text f = Button' img $ tip text $ act (ig >> f) $ spL
let btnFiles  = mkBtn Ico.files   "ファイルを開く" (openFiles false)
let btnFolder = mkBtn Ico.folder  "フォルダを開く" (openFiles true)
let btnSort   = mkBtn Ico.shuffle "ソート" sortSongs
let btnClear  = mkBtn Ico.clear   "クリア" lbxSong.Items.Clear
let btnStart  = mkBtn Ico.back    "バック" back
let btnPlay   = mkBtn Ico.play    "再生またはポーズします" playOrPause
let btnNext   = mkBtn Ico.next    "次へ" next


// events
isPlay =+ { btnPlay <*- isPlay.v.If Ico.pause Ico.play }

sliVol >< loaded =+ { me.Volume <- sliVol.vexp }

lbxSong.SelectionChanged => fun e ->
  for si:SongI in Seq.cast e.RemovedItems do 
    si.mpnt (_1 + (noMBtnDown.If 0.3 -0.3) >> max 0.)
  if noMBtnDown then
    lbxSong.ScrollIntoView lbxSong.item

tbxFilter.enter => next

let mutable byMouse = true
sliPos =+ {
  if me.hasTs && byMouse then
    me.po <- msec (me.NaturalDuration.TimeSpan.tmsec * sliPos.ratio)
  byMouse <- true }

lbxSong       => playSong
me.MediaEnded => next

mkTimer 100 =+ {
  lbxDl.Vis (lbxDl.cnt > 0)
  if me.hasTs then
    byMouse <- false
    sliPos <*- me.progress * sliPos.Maximum }

mkTimer 300 -% { V lbxSong.cnt } |> Observable.changed =+!  {
  do! 1000
  (Seq.cast<SongI> lbxSong.is).maxOf(fun x -> x.aw) |> _1 + 14. |> wnd.W }

trace true { 
wnd.Closing =+ { Seq.cast lbxSong.is |%> songIToFile |> pref.Ss "songs" }
pref.ss "songs" |> addFiles 
INIT_SHARED_VALUES
wnd.run
}
