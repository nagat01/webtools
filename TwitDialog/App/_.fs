open FStap open __ open System
[<product "TwitDialog"; version "0.5"; company "ながと"; copyright "Copyright © ながと 2015";
  title "TwitDialog: 文字を打つと、音声で返ってくる対話式のツイッタークライアント"; STAThread>] SET_ENG_CULTURE
open Speech
open AppCommands
open ClientCommands
open Rest
open Streaming
open Console

trace true {

AppCommands.Console.ini
ClientCommands.OAuth.ini
ClientCommands.Update.ini
Console.Console.ini
Speech.ini

cmdsAction.AddRange  [ getPIN; fav; rt; url; pause; restart; engage; current; history; tweet; reply; name; logout ]
cmdsTwitter.AddRange [ home; list; search; stream; liststream; user; favorite; ``new``; dm; mention; talk; track ]
cmdsSetting.AddRange [ voice; volume; speed; wait; tweetwait; time ]
cmdsBasic.AddRange   [ help; select; stop; about; save; load; what; exit ]

[ cmdsAction; cmdsTwitter; cmdsSetting; cmdsBasic ] |%| cmdsAll.AddRange 

wnd.Loaded =+ {
INIT_SHARED_VALUES 
tbx.Focus().ig
ClientCommands.OAuth.start
}

wnd.run 
}