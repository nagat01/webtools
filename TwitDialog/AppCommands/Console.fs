﻿module AppCommands.Console
open FStap open __

let ini =
  runCommand => fun (isInterrupt, line:s) ->
    let words = line.split " "
    if words.len > 0 then
      let cmd = words.[0]
      if isInterrupt then
        qOperations.cancel <*- ()
        qResponses.cancel <*- ()
      match cmdsAll.tryFind(fun __ -> __.isMatch cmd) with
      | Some command -> 
        command.action words
        if ( save.isMatch cmd || load.isMatch cmd || what.isMatch cmd ).n then
          prevCommand <*- line
      | _ ->
        enqf "%s と入力されましたが、%s というコマンドは割りあてられていません" cmd cmd
      qMessages.interrupt <*- ()

  tbx.enter =+ {
  runCommand <*- (true, tbx.v)
  tbx <*- "" 
  }
