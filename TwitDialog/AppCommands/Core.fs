﻿[<AutoOpen>]
module AppCommands.Core
open FStap open __

let cmdsBasic   :Command ra = ra()
let cmdsSetting :Command ra = ra()
let cmdsAction  :Command ra = ra() 
let cmdsTwitter :Command ra = ra() 
let cmdsAll     :Command ra = ra()

let runCommand = Event<b*s>()

