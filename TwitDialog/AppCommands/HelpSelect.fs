﻿[<AutoOpen>]
module AppCommands.HelpSelect
open FStap open __


let usage name up =
  sf @"%s と入力するだけで使えます。
    うえ矢印キーで %s
    した矢印キーで今のコマンドをもう１回読み上げます。
    ひだり矢印キーで前のコマンドを読み上げます。
    みぎ矢印キーで次のコマンドを読み上げます。
    また、%s 空白 basicで基本的なコマンドのみ読み上げます。
    %s 空白 settingで設定のコマンドのみ読み上げます。  
    %s 空白 actionでアプリを操作するコマンドのみ読み上げます。
    %s 空白 twitterでTwitter関連のコマンドのみ読み上げます。" 
      name up name name name name

let kindAction  () = CommandKind("ツイッタークライアントを操作する", cmdsAction)
let kindTwitter () = CommandKind("ツイートを読み上げる", cmdsTwitter)
let kindBasic   () = CommandKind("アプリを操作する", cmdsBasic)
let kindSetting () = CommandKind("アプリの設定をする", cmdsSetting)

let specifyKind speak otherwise (args:s[]) =
  match args with
  | String 1 ("action"  | "あくしょん")   -> kindAction  () |> speak
  | String 1 ("twitter" | "ついったー")   -> kindTwitter () |> speak
  | String 1 ("basic"   | "べーしっく")   -> kindBasic   () |> speak
  | String 1 ("setting" | "せってぃんぐ") -> kindSetting () |> speak
  | _ -> otherwise args

let speakKinds name speak =
  enqf "%sコマンドを、４つに分かれたカテゴリから選択できます。カテゴリを読み上げ中に、うえ矢印キーで選択できます。" name
  CommandKindSelector (
    speak, name,
    ra [ for kind in [ kindAction; kindTwitter; kindBasic; kindSetting ] -> kind () ] ) 
  |> qResponses.enq

 
let helpSpeak (kind:CommandKind) =
  enqf "%sコマンドについて、それぞれ短い説明をします。うえ矢印キーで読み上げているコマンドの詳しい説明が聞けます。" kind.name
  HelpSelector kind.commands |> qResponses.enq
   
let help =
  Command(
    "help", "へるぷ", 
    "コマンドの説明を読み上げます。",
    sf @"help に空白をあけて コマンド名 で、指定したコマンドの詳しい説明を読み上げます。
    %s" (usage "help" "読み上げ中のコマンドの詳しい説明を読み上げます。")
  , specifyKind helpSpeak (function
  | String 1 cmd ->
    match cmdsAll.tryFind(fun __ -> __.isMatch cmd) with
    | Some cmd ->
      enqf "%sコマンドは、%s" cmd.name cmd.brief 
      enqs cmd.usage.lines
    | _ -> 
      enqf "%sと入力されましたが、%sというコマンドは割り当てられていません" cmd.name cmd.name
  | _ -> 
    speakKinds "ヘルプを読み上げる" helpSpeak)
  )


let speakSelect (kind:CommandKind) =
  enqf "%sコマンドを読み上げます。テキストボックスに入力するコマンドを、うえ矢印キーで選択できます。" kind.name
  SelectSelector kind.commands |> qResponses.enq
 
let select =
  Command(
    "select", "せれくと",
    "コマンドを読み上げから選択して、テキストボックスに入力できます",
    usage "select" "読み上げ中のコマンドをテキストボックスに入力できます。",
    specifyKind speakSelect 
      (fun _ -> 
        speakKinds "テキストボックスに入力する" speakSelect )
  )

