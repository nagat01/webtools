﻿[<AutoOpen>]
module AppCommands.SaveLoad
open FStap open __

let private usage name desc =
  sf @"%s 空白 数字 で、%s
  数字は 1 ~ 99 までの範囲で指定してください。" name desc

let private numberCommand name run usage (args:s[]) =
  let line = args.concat " "
  match args with
  | Int 1 n ->
    if 0 < n && n < 100 then
      run n
    else
      enqf "%s と入力されましたが、数字は 1 から 99 の範囲で指定してください。" line
  | _ -> 
    enqf "%s と入力されましたが、%sの後に空白をあけて、数字を指定してください。" line name

let save =
  Command(
    "save", "せーぶ",
    "前回のコマンドを保存できます。",
    usage "save" "前回のコマンドを指定した数字で呼び出せるように保存できます。"
  ).withUsage <| numberCommand "save" (fun n ->
    match prevCommand with
    | Some' line ->
      pref.["cmd" + n.s] <- line
      enqf "コマンド %s を 数字 %d に保存しました。" line n
    | _ -> 
      enq "まだコマンドが入力されていません。"
    )


let load =
  Command(
    "load", "ろーど",
    "保存されているコマンドを呼び出せます。",
    usage "load" "指定した数字に保存されたコマンドを呼び出せます。"
  ).withUsage <| numberCommand "load" (fun n ->
    match pref.[ "cmd" + n.s] with
    | Some line -> 
      enqf "数字%dに保存されているコマンド、%sを実行します。" n line
      runCommand <*- (false, line)
    | _ -> 
      enqf "数字%dにコマンドは保存されていませんでした。" n
    )


let what =
  Command(
    "what", "わっと",
    "保存されているコマンドを読み上げます。",
    usage "what" "指定した数字に保存されたコマンドを読み上げます。"
  ).withUsage <| numberCommand "what" (fun n ->
    match pref.["cmd" + n.s] with
    | Some line -> 
      enqf "数字%dに保存されているコマンドは%sです。" n line
    | _ -> 
      enqf "数字%dにコマンドは保存されていませんでした。" n
    )

