﻿module Client
open FStap open __ open System
open CoreTweet

type Params = Dictionary<s, o>

let mkParams ps = Params(dict[for k, v in ps -> k, box v])

let isTimeStamp = pref.shB "isTimeStamp" true

type DateTimeOffset with
  member __.elapsed = 
    let ts = now - __.LocalDateTime
    if ts.Days > 0 then
      let __ = __ + TimeSpan.FromHours 9.
      sf "%d月%d日" __.Month __.Day
    elif ts.tmin >= 1. then
      If (ts.Hours   > 0) (sf "%d時間" ts.Hours) "" +
      If (ts.Minutes > 0) (sf "%d分"   ts.Minutes) "" + "前"
    else
      If (ts.Seconds > 0) (sf "%d秒前" ts.Seconds) ""

type Status with
  member private __.sCount (n:i Nullable) f = if (n.HasValue && n.Value > 0) then f n.Value else ""
  member __.fav     = __.sCount __.FavoriteCount (sf "ふぁぼ%d個")
  member __.rt      = __.sCount __.RetweetCount  (sf "リツイート%d回")
  member __.elapsed = __.CreatedAt.elapsed
  member __.sElapsed = If isTimeStamp.v (sf "%s、" __.elapsed) ""
  member __.textOnly = __.Text.rep "RT @" "リツイート @"
  member __.textNoname = sf "%s%s、%s、%s。" __.sElapsed __.textOnly __.fav __.rt
  member __.text = sf "%sさん、%s" __.User.Name __.textNoname

type DirectMessage with
  member __.elapsed = __.CreatedAt.elapsed
  member __.text = 
    sf "%sさんから、%s%s" 
      __.Sender.Name 
      ( If isTimeStamp.v (sf "%s、" __.elapsed) "")
      __.Text 

let curTweet = None'<Status>
let historyTweets = Queue<Status>()

let withTokens f args =
  match tokens with
  | Some' tokens -> f tokens args
  | _ ->
    enq "ツイッタークライアントが許可されていません"
    enq "getpinでPIN認証をおこなってください"

let enqHistory (__:Status) =
  if historyTweets.Contains(__).n then
    historyTweets.enqTruncate __ 1000
  curTweet <*- __

let enqListSelector user (lists:List seq) enqListSpeech =
  let lists = ra lists
  if lists.len > 0 then
    enqf "%sのリストが%d個見つかったので、読み上げます。リスト名の読み上げ中にうえ矢印キーを押すと、読み上げるリストを選択できます。" user lists.len
    ListSelector(lists, enqListSpeech user) |> qResponses.enq

let userNotFound user =
  enqf "ユーザーID %s と指定されましたが、そのようなユーザーは見つかりませんでした。" user