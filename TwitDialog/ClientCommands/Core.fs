﻿[<AutoOpen>]
module ClientCommands.Core
open FStap open __


let confirm textConfirm yes no fYes textYes textNo = async {
  qMessages.isPlaying <- false
  enq textConfirm 
  enqf "%sなら y, %sなら それ以外のキーを押してください。" yes no
  let! e = wnd.kdown
  qOperations.cancel <*- ()
  qResponses.cancel <*- ()
  if e.Key = Key.Y then
    fYes() |> textYes |> enq
  else
    textNo |> enq
  qMessages.isPlaying <- true
  immediate' {
  do! 20
  tbx <*- "" }
  }

