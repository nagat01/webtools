﻿[<AutoOpen>]
module ClientCommands.Engagement
open FStap open __
open Client

let usage name action action2 =
  sf @"%s と打つだけで使えます。
    読み上げられた最新のツイートがあれば、そのツイートを%sかもう一度確認されます。
    そこでyと打つと実際に%s、それ以外のキーを押すと%sません。"
    name action action2 action2


let noTweet name =
  enqf "%sコマンドは、読み上げられた最新のツイートがあった時だけ使えます。" name 

let fav =
  Command(
    "fav", "ふぁぼ",
    "読み上げられた最新のツイートをふぁぼります。",
    usage "fav" "ふぁぼる" "ふぁぼられ"
, withTokens <| fun tokens _ ->
  match curTweet with
  | Some' status -> immediate' { do!
    confirm 
      (sf "%sさんのツイート %s をふぁぼりますか？" status.User.Name status.textOnly) "ふぁぼる" "ふぁぼらない"
      (fun _ -> tokens.Favorites.Create(id = status.Id))
      (fun _ -> "ツイートをふぁぼりました。")
      "ツイートをふぁぼりませんでした。"
    }
  | _ -> noTweet "fav"
 )  


let rt =
  Command(
    "rt", "りついーと",
    "読み上げられた最新のツイートをりついーとします。",
    usage "rt" "リツイートする" "リツイートされ"
, withTokens <| fun tokens _ ->
    match curTweet with
    | Some' status -> immediate' { do! 
      confirm 
        (sf "%sさんのツイート %s をりついーとしますか？" status.User.Name status.textOnly) "りついーとする" "りついーとしない" 
        (fun _ -> tokens.Statuses.Retweet(id = status.Id).ig)
        (fun _ -> "ツイートをりついーとしました。")
        "ツイートをりついーとしませんでした。"
      }
    | _ -> noTweet "rt"
  )


let url =
  Command(
    "url", "ゆーあーるえる",
    "ツイートに含まれるURLを選択して、ブラウザから開くことができます。",
    "url で、読み上げられた最新のツイートに含まれるURLを読み上げから選択して、ブラウザで開くことができます。", fun _ ->
  match curTweet with
  | Some' status ->
    let urls = ra status.Entities.Urls
    if urls.len > 0 then
      enqf "読み上げられたツイートに %d 個、URL が見つかりました。うえ矢印キーで読み上げられている URL を選択して、ブラウザで開くことができます。" urls.len
      UrlSelector urls |> qResponses.enq
    else
      enq "読み上げられたツイートに含まれるURLは見つかりませんでした。"
  | _ -> noTweet "ゆーあーるえる"
  )

