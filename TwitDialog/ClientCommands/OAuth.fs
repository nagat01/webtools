﻿[<AutoOpen>]
module ClientCommands.OAuth
open FStap open __ open System open System.Windows
open CoreTweet
open Contract


let _accessToken       = pref.shS "accessToken" ""
let _accessTokenSecret = pref.shS "accessTokenSecret" ""
let _screenName        = pref.shS "userScreenName" ""
let _userId            = pref.shI64 "userId" 0L

let oauth = OAuth.Authorize(consumerKey, consumerSecret)
let gettingPIN = None'<s>

let login<'__> = 
  try
    match _accessToken.v, _accessTokenSecret.v with
    | "", "" -> 
      false
    | accessToken, accessTokenSecret ->
      tokens <*- Tokens.Create(consumerKey, consumerSecret, accessToken, accessTokenSecret)
      tokens |%| fun tokens ->
        tokens.UserId <- _userId.v
        tokens.ScreenName <- _screenName.v
        enqf "ユーザーID %sで、twitterにログインしました" tokens.ScreenName
      true
  with _ -> 
    enq "twitterへのログインに失敗しました"
    false

let start<'__> =
  match now.Hour with
  | LT 4  -> "夜遅くまでこんばんは"
  | LT 7  -> "おはようございます。早朝ですね"
  | LT 11 -> "おはようございます"
  | LT 14 -> "こんにちは、お昼時ですね"
  | LT 18 -> "こんにちは"
  | LT 20 -> "こんばんは、夕食時ですね"
  | LT 23 -> "こんばんは"
  | _     -> "こんばんは、眠い時間ですね"
  |> enq
  enq "ツイットダイアログを起動しました。"
  if login then
    enq "about で最初に必要な説明が聞けます"
  else
    enq "about で最初に必要な説明が聞けます"
    enq "getpin で、ツイッタークライアントを許可することが出来ます"

let mutable private nFailed = 0
let checkPIN() =
  try
    let pin = Clipboard.GetText()
    if 
      gettingPIN.is((<>)pin) && 
      6 < pin.len && pin.len < 9 && 
      pin.forall Char.IsDigit
    then
      let _tokens = oauth.GetTokens pin
      tokens <*- _tokens
      _accessToken <*- _tokens.AccessToken
      _accessTokenSecret <*- _tokens.AccessTokenSecret
      _screenName <*- _tokens.ScreenName
      _userId <*- _tokens.UserId
      enq "PIN認証に成功しました"
      gettingPIN.none
      login.ig
  with _ -> 
    nFailed <- nFailed + 1 
    if nFailed > 5 then failwith "PIN認証に繰り返し失敗しました。"


let getPIN = 
  Command(
    "getpin", "げっとぴん", 
    "PIN認証のためのページを開きます", 
    @"getPIN と入力するだけで使えます。
      コマンドを入力すると、ブラウザでPIN認証のためのページが開かれます。
      アプリを許可して、ページにあるPINコードをクリップボードにコピーすると、PIN認証されます。"
  , fun _ ->
    gettingPIN <*- Clipboard.GetText()
    processPWD oauth.AuthorizeUri.AbsoluteUri
    enq "PIN認証のためのページを開きます。"
    enq "アプリを許可して、PINコードをクリップボードにコピーすると、PIN認証されます"
  )

let logout =
  Command(
    "logout", "ろぐあうと",
    "ツイッターからログアウトできます。",
    "logoutと入力するだけで使えます。"
  , fun _ -> 
    enq "ツイッターからログアウトしました。" 
    tokens.none
    qMessages.cancel <*- ()
    _accessToken <*- ""
    _accessTokenSecret <*- ""
    _screenName <*- ""
    _userId <*- 0L
 )


let ini =
  timer10ms =+ { checkPIN () }

