﻿[<AutoOpen>]
module ClientCommands.Operations
open FStap open __ open System
open CoreTweet
open Client

let followings = None'<User[]>


let current =
   Command(
    "current", "かれんと",
    "読み上げられた最新のツイートを読み上げます。",
    "current と打つだけで使えます。", fun _ ->
  match curTweet with
  | Some' status ->
    enq "読み上げられた最新のツイート、"
    enq status.text
  | _ ->
    enq "読み上げられた最新のツイートはありませんでした。"
  )


let history = 
  Command(
    "history", "ひすとりー",
    "読み上げられたツイートの履歴を読み上げます。",
    "history と打つだけで使えます。", fun _ ->
  let len = historyTweets.len
  if len > 0 then
    qMessages.cancel <*- ()
    sf "読み上げられたツイートの履歴が%d個ありましたので読み上げます。" len 
    |> enqMessage
    Tweets(
      ra historyTweets.rev, 
      0, 
      (fun status -> status.Text),
      (fun status -> curTweet <*- status)
      , ig)
    |> qMessages.enq 
  else
    enq "読み上げられたツイートの履歴はありませんでした。"
  )


let name = 
  Command(
    "name", "ねーむ",
    "ユーザーのIDまたは名前を検索して、ユーザーIDをテキストボックスに入力できます。",
    @"name で、自分がフォローしているユーザーから、ユーザーIDを選択して、テキストボックスに入力できます。
    name に空白をあけて ユーザーのIDまたは名前の一部分 で、指定した文字列を含むユーザーIDを選択して、テキストボックスに入力できます。
    ただし、自分が最後にフォローした500人のユーザーの中から、ユーザーを検索します。"
, withTokens <| fun tokens args ->
  let id = tokens.UserId
  let n  = tokens.Users.Show(user_id = id).FriendsCount
  let ids = tokens.Friends.EnumerateIds(EnumerateMode.Next, user_id = id, count = Nullable n)
  let users = 
    match followings with
    | Some' users -> users
    | _ ->
      let users = [|
        for ids in Seq.chunkBySize 100 ids |> Seq.truncate 5 do 
          yield! tokens.Users.Lookup(user_id = ids) |]
      followings <*- users
      users
  let str = match args with String 1 str -> str | _ -> ""
  let users =
    if str = "" then users 
    else users |> Array.filter(fun user -> user.ScreenName.Contains str || user.Name.Contains str)
  if users.len > 0 then
    let desc = "ユーザー名を読み上げ中に、うえ矢印キーでユーザーIDをコメント欄に入力できます。"
    if str = "" then
      enqf "ユーザーの候補が%d人見つかったので読み上げます。%s" users.len desc
    else
      enqf "条件 %s に一致するユーザーの候補が%d人見つかったので読み上げます。%s" str users.len desc
    UserSelector (ra users) |> qResponses.enq 
  else
    if str = "" 
    then enq "ユーザーの候補は見つかりませんでした。"
    else enqf "条件 %s に一致するユーザーの候補は見つかりませんでした。" str
  )

