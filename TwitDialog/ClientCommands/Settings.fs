﻿[<AutoOpen>]
module ClientCommands.Settings
open FStap open __
open Client

let run name (value:MessageSpeaking) state _ =
  if qMessages.Count > 0 then
    if messageSpeaking <>. value then
      enqf "%sしました。" state
      messageSpeaking <*- value
    else
      enqf "%sと入力されましたが、すでに%sなっています。" name state
  else
    enqf "%sコマンドは、ツイートの読み上げ中にのみ使用できます。" name


let pause =
  Command(
    "pause", "ぽーず",
    "１つのツイートの読み上げ後、キー入力があるまで次のツイートを読み上げないようにできます。",
    "pauseと入力するだけで使えます。"
  , run 
    "pause" MessageSpeaking.Pause
    "１つのツイートの読み上げ後、キー入力があるまで次のツイートを読み上げないように" 
  )

let restart =
  Command(
    "restart", "りすたーと",
    "１つのツイートの読み上げ後、そのまま次のツイートを読み上げるようにできます。",
    "restartと入力するだけで使えます。"
  , run 
    "restart" MessageSpeaking.Play
    "１つのツイートの読み上げ後、そのまま次のツイートを読み上げるように" 
  )

let engage =
  Command(
    "engage", "えんげーじ",
    "１つのツイートの読み上げ後、次のツイートを読み上げないようにできます。また、みぎ矢印キーとひだり矢印キー入力で次のツイート、前のツイートに移動しないようになります。",
    "engageと入力するだけで使えます。"
  , run 
    "engage" MessageSpeaking.Engage
    "１つのツイートの読み上げ後、次のツイートを読み上げないようにしました。また、みぎ矢印キーとひだり矢印キーで次のツイート、前のツイートに移動しないように" 
  )


let time =
  Command(
    "time", "たいむ",
    "ツイート読み上げ時に、そのツイートが投稿されてから現在までの経過時間を読み上げるかどうかを設定できます。",
    @"time に空白をあけて on で、ツイート読み上げ時に、ツイートが投稿されてから現在までの経過時間を読み上げるようにします。
      time に空白をあけて off で、ツイート読み上げ時に、ツイートが投稿されてから現在までの経過時間を読み上げないようにします。"
  ).withUsage <| fun usage -> 
  function
  | String 1 ( "on" | "おん") -> 
    if isTimeStamp.v then
      enq "time on と入力されましたが、すでにツイートが投稿されてからの経過時間を読み上げるようになっています。"
    else
      isTimeStamp <*- true
      enq "ツイートが投稿されてからの経過時間を読み上げるようにしました。"
  | String 1 ("off" | "おふ") ->
    if isTimeStamp.v then
      isTimeStamp <*- false
      enq "ツイートが投稿されてからの経過時間を読み上げないようにしました。"
    else
      enq "time off と入力されましたが、すでにツイートが投稿されてからの経過時間を読み上げないようになっています。"
  | line ->
    enqf "%sと入力されましたが、" (line.concat " ")
    enq usage

