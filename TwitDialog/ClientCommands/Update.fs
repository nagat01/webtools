﻿[<AutoOpen>]
module ClientCommands.Update
open FStap open __ open System
open System.Globalization
open System.Text.RegularExpressions
open CoreTweet
open Client

[<RequireQualifiedAccessAttribute>]
type PostedTextBox = Tweet | Reply of Status
let postedTextBox:PostedTextBox opt' = None'

let getText (words:s[]) = 
  (Seq.tail words).concat " "

let getTextReply (tweet:Status) text =
  "@" + tweet.User.ScreenName + " " + text

let countText (text:s) =
  StringInfo(text).LengthInTextElements 
  - Regex.Matches(text, @"\r\n").Count
  + Seq.sum [ for m in Regex.Matches(text, patURL) -> 23 - m.Length ]


let inputText (userToReply:Status opt) args (confirm:s -> u a) = async {
  let addReplyTarget text =
    match userToReply with 
    | Some tweet -> getTextReply tweet text
    | _ -> text
  let isTweet = userToReply.bad
  match args with
  | [|_|] ->
    let name = If isTweet "ツイート" "リプライ"
    enqf "テキストボックスを%s投稿欄に切り替えました。" name
    enqf "Tabキーを押すと%sを投稿できます。" name
    enqf "Tabキーを押した後で%sする内容が読み上げられ、投稿するかを確認されます。" name
    enqf "Shiftキーを押しながらTabキーを押すと、%sの作成を中止し、%s投稿欄を閉じます。" name name
    enqf "エンターを押して改行ありの%sを作成することもできます。" name
    enqf "F1キーを押すと、%sの文字数と内容が読み上げられます。" name
    qMessages.isPlaying <- false
    tbx.AcceptsReturn <- true
    postedTextBox <*- 
      match userToReply with
      | Some tweet -> PostedTextBox.Reply tweet
      | _ -> PostedTextBox.Tweet 
    let restore() =
      qMessages.isPlaying <- true
      tbx.AcceptsReturn <- false     
      postedTextBox.none
    while postedTextBox.ok do
      let! isCancel = tbx.kd Key.Tab -% { V IsShift }
      qOperations.cancel <*- ()
      qResponses.cancel <*- ()
      if isCancel then
        tbx <*- ""
        enqf "%s投稿欄を閉じ、%sの作成を中止しました。" name name
        restore()
      else
        let text = addReplyTarget tbx.v
        let count = countText text
        if count > 140 then
          enqf "%sの文字数が%d文字のため、%sできません。140文字以内にすると投稿できます。" name count name
        else
          do! confirm text
          restore()
  | _ -> 
    do! confirm (addReplyTarget (getText args))
  }


let tweet =
  Command(
    "tweet", "ついーと",
    "ツイートを投稿できます。",
    @"tweetの後ろに空白をあけて、ツイートの文章を書いて、ツイートを投稿できます。
      tweetのみで、テキストボックスをツイート投稿欄に切り替えます。
      ツイート投稿欄ではエンターで改行、Tabで内容を確定、Shiftを押しながらTabで投稿を中止、F1キーで文字数と書いている内容を読み上げます。"
, withTokens <| fun tokens args -> immediate' { 
  do! inputText None args (fun text ->
  confirm 
    (sf "%d文字のツイート %s を投稿しますか？" (countText text) text)
    "投稿する" "投稿しない"
    (fun _ -> 
      try tokens.Statuses.Update(status = text) |> Choice1Of2 
      with e -> e.Message |> Choice2Of2 )
    (function
      | Choice1Of2 res -> sf "ユーザー %s として、%s と投稿しました。" res.User.Name res.Text
      | Choice2Of2 res -> sf "ツイートの投稿に失敗しました。%s" res )
    (sf "ツイートを投稿しませんでした") 
  ) }
  )


let reply =
  Command(
    "reply", "りぷらい",
    "読み上げられた最新のツイートを宛先とするリプライを投稿できます。",
    @"reply の後ろに空白をあけて、リプライの文章を書いて、リプライを投稿できます。
      replyのみで、テキストボックスをリプライ投稿欄に切り替えます。
      リプライ投稿欄ではエンターで改行、Tabで内容を確定、Shiftを押しながらTabで投稿を中止、F1キーで文字数と書いている内容を読み上げます。
      リプライの先頭に @リプライ先のユーザーIDが入ります。"
, withTokens <| fun tokens args ->
  match curTweet with
  | Some' tweet -> immediate' {
  do! inputText (Some tweet) args (fun text ->
  confirm 
    (sf "%d文字の%sさん宛のリプライ%sを投稿しますか？" (countText text) tweet.User.Name text)
    "投稿する" "投稿しない"
    (fun _ ->
      try 
        tokens.Statuses.Update(
          status = text, 
          in_reply_to_status_id = Nullable tweet.Id) |> Choice1Of2
      with e -> 
        e.Message |> Choice2Of2 )
    (function
      | Choice1Of2 res ->
        sf "ユーザー %s として %sさんツイート %s に %s とリプライを投稿しました。" 
          res.User.Name tweet.User.Name tweet.textNoname res.Text 
      | Choice2Of2 res -> 
        sf "リプライの投稿に失敗しました。%s" res)
    "リプライを投稿しませんでした。"
    ) }
  | _ ->
    enq "読み上げられた最新のツイートが見つからなかったため、リプライを投稿できませんでした。"
  )


let speechPostText postedTextBox text =
  match postedTextBox with
  | PostedTextBox.Tweet -> "ツイート", text
  | PostedTextBox.Reply tweet -> "リプライ", getTextReply tweet text
  |> fun (name, text) ->
    enqOperation (sf "いま書いている%sの文字数は%d文字です。" name (countText text))

let ini = 
  wnd.kd Key.F1 =+ {
  qOperations.cancel <*- ()
  qResponses.interrupt <*- ()
  qMessages.interrupt <*- ()
  let words = tbx.v.split " "
  match postedTextBox with
  | Some' postedTextBox ->
    speechPostText postedTextBox tbx.v
  | _ -> 
    if words.len > 0 then 
      let text = getText words
      if tweet.isMatch words.[0] then 
        speechPostText PostedTextBox.Tweet text
      elif reply.isMatch words.[0] then
        match curTweet with
        | Some' tweet -> speechPostText (PostedTextBox.Reply tweet) text
        | _ -> ()
  if words.len > 0 then
    enqOperation tbx.v
  else
    enqOperation "いまはテキストボックスに何も入力されていません。"
}

