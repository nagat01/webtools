﻿[<AutoOpen>]
module Console.Console
open FStap open __

let loop (q:_ SpeechQueue) (run:_ -> u a) = async {
  while q.Count > 0 do 
    let item = q.Peek()
    do! run item
    if q.Count > 0 && q.Peek() = item then
      q.deq.ig 
  }

let ini =
  let restart (qSource:_ SpeechQueue) (qTarget:_ SpeechQueue) = async {
    if qSource.len = 0 then
      qTarget.restart <*- () }

  immediateLoop' 10 {
  do! loop qOperations (function
    | :? Operation as __ -> speak qOperations __.text |> Async.Ignore
    | _ -> async {()} )
  do! restart qOperations qResponses
  }

  immediateLoop' 10 {
  do! qResponses.restart 
  do! loop qResponses Responses.run
  do! restart qResponses qMessages
  }

  immediateLoop' 10 {
  do! qMessages.restart
  do! loop qMessages Messages.run
  }

  qMessages.cancel =+ { 
  messageSpeaking <*- MessageSpeaking.Play 
  }


let exit =
  Command(
    "exit", "いぐじっと", 
    "ツイットダイアログを終了できます。", 
    "exitと入力すれば、ツイットダイアログを終了できます。"
  , fun _ -> immediate' {
  qMessages.cancel <*- ()
  do! 10
  let _, finish = speakPrimitive "ツイットダイアログを終了します。"
  let! _ = finish
  wnd.Close() }
  )