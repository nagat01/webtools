﻿[<AutoOpen>]
module Console.Core
open FStap open __ open System 

let _replace   = pref.shS "単語の置き換え" "＠,あっと;@,あっと;www,;ｗｗｗ,;WWW,;WWW,;#,ハッシュタグ;＃,ハッシュタグ;♯,ハッシュタグ"
let _remove    = pref.shS "読み上げない文字" "()（）･´｀ω+ﾉ!:/・"

let patternNotURL =
  "(since until from to near within geocode lang filter min_retweets min_faves min_replies:)"
    .rep " " ":|"

let isURL (word:s) =
  (word.urlOk && word.isMat(patternNotURL).n)

let educateTts (text:s) =
  let replace = try _replace.v.kvs with _ -> [||]
  let remove  = _remove.v
  (text.repRegex patURL (text.isEnglish.If "url" "ゆーあーるえる省略"))
    .repsRegex(replace)
    .filter(remove.have.n)
    |> String.Concat


type InputResult = Finish | Cancel | Interrupt | Up | Down | Right | Left
 
type SpeechQueue<'a when 'a :> SpeechBase> with
  member __.cancelOrInterrupt = __.cancel -%% Cancel >=< __.interrupt -%% Interrupt 

let keyUp    = wnd.pkd Key.Up    -%% Up
let keyDown  = wnd.pkd Key.Down  -%% Down
let keyRight = wnd.pkd Key.Right -%% Right
let keyLeft  = wnd.pkd Key.Left  -%% Left

let speakPrimitive (text:s) =
  let speech = text.isEnglish.If english japanese 
  let prompt = educateTts text |> speech.speak  
  speech, speech.speech.SpeakCompleted -?? (fun __ -> __.Prompt = prompt) -%% Finish

let cancelSpeaking (__:Speech') =
    __.speech.SpeakAsyncCancelAll()

let waitIfInterrupt (q:_ SpeechQueue) prevResult = async {
  match prevResult with
  | Interrupt -> 
    let! result = 
      q.cancel -%% Cancel >=< 
      q.restart -? { V q.isPlaying } -%% Interrupt
    return result
  | _ -> 
    return prevResult
  }

let sleep (q:_ SpeechQueue) (time:f) = async {
  if time > 0.0 then
    let start = now
    let! result = 
      timer10ms -? { V ((now - start).tsec > time) } -%% Finish >=<
      keyRight >=< 
      q.cancelOrInterrupt
    let! result = waitIfInterrupt q result
    return result
  else
    return Finish
  }

let rec speak (q:_ SpeechQueue) text = async {
  let speech, finish = speakPrimitive text
  let! result = 
    ( finish >=< 
      keyRight >=< keyDown >=<
      q.cancelOrInterrupt )
  cancelSpeaking speech
  let! result = waitIfInterrupt q result
  match result with
  | Down | Interrupt -> return! speak q text
  | Finish -> return! sleep q _wait.v
  | _ -> return result
  }

let speakInputWaiting (q:_ SpeechQueue) (filter:_->b) text = async {
  let speech, finish = speakPrimitive text
  let! result =
    ( finish >=<
      keyUp >=< keyDown >=< keyRight >=< keyLeft >=<
      q.cancelOrInterrupt
    ) -?? filter
  cancelSpeaking speech
  return! waitIfInterrupt q result
  }

