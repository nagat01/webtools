﻿module Console.Messages
open FStap open __ 
open CoreTweet open CoreTweet.Streaming

let filterArrow __ =
  ( __ <> Finish || messageSpeaking =. MessageSpeaking.Play ) &&
  ( ( __ <> Right && __ <> Left) || messageSpeaking <>. MessageSpeaking.Engage ) 

let speak text = speak qMessages text

let speakInputWaiting text = speakInputWaiting qMessages filterArrow text


let speakMessages fUp (__:'a Messages) = async {
  let mutable next = true
  let onNext() =
    next <- __.next
    if next.n then __.onFinished
  while next do
    let message = __.current
    __.onSpeak message
    let! result = speakInputWaiting (__.toText message)
    match result with
    | Finish ->
      onNext()
      let! result = sleep qMessages _tweetwait.v
      if result = Cancel then next <- false
    | Cancel -> next <- false
    | Up    -> (fun _ -> fUp message)()
    | Down | Interrupt -> ()
    | Right -> onNext()
    | Left  ->
      do! __.prev.n
      let! result = speak (sf "それ以上前の%sを読むことは出来ません" __.name)
      if result = Cancel then next <- false
  }


let speakStream fUp (__:Stream) = async {
  let mutable textOutside = None
  let mutable next = true
  while next do
    if __.index < __.count then
      let message = __.current
      __.onSpeak message
      let! result = speakInputWaiting (__.toText message)
      match result with
      | Finish ->
        __.nextForce.ig
        let! result = sleep qMessages _tweetwait.v
        if result = Cancel then next <- false
      | Cancel -> next <- false
      | Up    -> (fun _ -> fUp message)()
      | Down | Interrupt -> ()
      | Right -> 
        do! __.nextForce.n
        textOutside <- Some (sf "それ以上新しい%sは見つかりませんでした。" __.name)
      | Left ->
        do! __.prev.n
        let! result = speak (sf "それ以上前の%sを読むことは出来ません" __.name)
        if result = Cancel then next <- false
    else
      let! result = 
        match textOutside with 
        | Some text -> 
          textOutside <- None
          speakInputWaiting text
        | _ -> async {
          let! result = ( 
            __.received -%% Finish >=<
            qMessages.cancel -%% Cancel >=<
            wnd.pkd Key.Left -%% Left >=<
            wnd.pkd Key.Right -%% Right ) -?? filterArrow
          return result }
      match result with
      | Finish -> ()
      | Cancel -> next <- false
      | Up | Down | Interrupt -> ()
      | Right ->
        do! __.next.n 
        textOutside <- Some (sf "それ以上新しい%sは見つかりませんでした。" __.name)
      | Left -> 
        do! __.prev.n
        textOutside <- Some (sf "それ以上前の%sを読むことは出来ません" __.name)
  __.disposer.Dispose()
    }

let openStatus (status:Status) =
  try
    processPWD (sf "http://twitter.com/%s/status/%d" status.User.ScreenName status.Id) 
    enqf "%sさんのツイートをブラウザで開きました。" status.User.Name
  with _ ->
    enqf "%sさんのツイートをブラウザで開くことが出来ませんでした。" status.User.Name

let run (__:MessageBase) =
  match __ with
  | :? Message as __ -> speak __.text |> Async.Ignore
  | :? Tweets  as __ -> __ |> speakMessages openStatus
  | :? DMs     as __ -> __ |> speakMessages (fun _ -> ())
  | :? Stream as __ ->
    __ |> speakStream (function
      | :? StatusMessage as __ -> openStatus __.Status
      | _ -> ()
      )

  | _ -> async{()}

