﻿module Console.Responses
open FStap open __

let speak text = speak qResponses text

let speakInputWaiting text = speakInputWaiting qResponses (fun _ -> true) text


let speakSelector getCurrent (onSelected:_ -> InputResult a) (__:_ Selector) = async {
  let mutable next = true
  let onNext = async {
    let text = sf """
      すべての%sを読み上げ終わりました。
      最初の%sから繰り返したい場合にはうえ矢印キーを、
      最後の%sに戻りたい場合はひだり矢印キーを押してください。""" __.name __.name __.name
    if __.next.n then
      let mutable repeat = true
      while repeat do
        repeat <- false
        let! result = speakInputWaiting text 
        match result with
        | Finish | Cancel | Right -> 
          next <- false
        | Up -> 
          __.index <- 0
          next <- true
        | Down | Interrupt -> repeat <- true
        | Left -> next <- true }
 
  while next do
    let text, item = getCurrent()
    let! result = speakInputWaiting text
    match result with
    | Finish -> 
      do! onNext
      let! result = sleep qResponses _wait.v
      if result = Cancel then next <- false
    | Cancel -> next <- false
    | Up -> 
      let! result = onSelected item
      if result <> Cancel then
        match __ :> obj with 
        | :? HelpSelector -> do! onNext
        | _ -> next <- false
      else
        next <- false
    | Down | Interrupt -> ()
    | Right -> do! onNext
    | Left  ->
      do! __.prev.n
      let! result = speak (sf "それ以上前の%sを読み上げる事は出来ません" __.name)
      next <- result <> Cancel
  }


let run (response:ResponseBase) =
  match response with
  | :? Response as text -> speak text.text |> Async.Ignore

  | :? HelpSelector as __ ->
    __ |> speakSelector 
      (fun _ ->
        let cmd = __.current
        let text = sf "%sコマンドは、%s" cmd.name cmd.brief
        text, cmd )
      (fun cmd ->
        speak (sf "%s コマンドについて詳しく説明します。%sコマンドは、%s %s" cmd.name cmd.name cmd.brief cmd.usage) )

  | :? SelectSelector as __ ->
    __ |> speakSelector
      (fun _ ->
        let name = __.current.name
        let text = sf "%s コマンドを入力しますか？" name
        text, name )
      (fun name ->
        tbx <*- name
        tbx.Select(tbx.v.len, 0)
        speak (sf "%s コマンドをテキストボックスに入力しました。" name) )

  | :? CommandKindSelector as __ ->
    __ |> speakSelector
      (fun _ ->
        let kind = __.current
        let name = kind.name
        let text = sf "%sコマンドを選択しますか？" name
        text, kind )
      (fun kind -> 
        __.speak kind
        async { return Cancel } )
        
  | :? VoiceSelector as __ ->
    __ |> speakSelector
      (fun _ ->
        let name = __.current.shortName
        let text = sf "%sを読み上げ音声に選択しますか？" name
        text, name )
      (fun name ->
        __.speech.selVoice name
        pref.[sf "voice-%s" __.speech.culture] <- name
        speak (sf "%s を読み上げ音声に設定しました。" name) )

  | :? ListSelector as __ ->
    __ |> speakSelector
      (fun _ ->
        let list = __.current
        let text = sf "リスト %s を選択しますか？" list.Name
        text, list )
      (fun list -> async { 
        prevCommand <*- sf "list %s %s" list.User.ScreenName (list.Name.rep " " "-")
        __.speakList list 
        return Finish })

  | :? UserSelector as __ ->
    __ |> speakSelector
      (fun _ ->
        let user = __.current
        let text = sf "%sさん@%s を選択しますか？" user.Name user.ScreenName
        text, user )
      (fun user ->
        tbx <*- user.ScreenName
        tbx.SelectionLength <- 0
        tbx.SelectionStart <- 0 
        speak (sf "%s さんのユーザーID %s をテキストボックスに入力しました。文字入力のカーソルを先頭に移動しました。" user.Name user.ScreenName)
        )

  | :? UrlSelector as __ -> 
    __ |> speakSelector
      (fun _ ->
        let url = __.current
        let desc = sf "%dつ目のゆーあーるえる" (__.index + 1)
        let text = sf "%sをブラウザで開きますか？" desc
        text, (desc, url.ExpandedUrl) )
      (fun (desc, url) ->
        processPWD url
        speak (sf "%s をブラウザで開きました。" desc ) )

  | _ -> async{()}

