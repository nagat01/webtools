﻿[<AutoOpen>]
module __.Core
open FStap open System.Windows
open CoreTweet

type Command(name:s, japanese:s, brief:s, usage:s, ?action:s[]->u) =
  member __.name  = name
  member __.brief = brief
  member __.usage = sf "%s\r\n%sは、ひらがなで %sと書くこともできます。" usage name japanese
  member __.isMatch cmd = name = cmd || japanese = cmd
  member val action = action |? ig with get, set
  member __.withUsage f =
    __.action <- f usage
    __

type CommandKind(name:s, commands:Command ra) = 
  member __.name = name
  member __.commands = commands


let parse parse n (__:s[]) = if __.len > n then parse __.[n] else None
let (|Float |_|) n __ = parse (f.TryParse >> tupleToSome) n __
let (|Int   |_|) n __ = parse (i.TryParse >> tupleToSome) n __
let (|String|_|) n __ = parse Some n __

// url characters: !#$%&'()=-~[]*;+:_,./?
let patURL = @"(https?)://[\w!#\$%&'\(\)=-@\~\[\]\*;\+:,\./\?]+"

let wnd  = Window(Title = "TwitDialog") $ wh 600 400 $ centerScreen
let  tbx = TextBox' () $ wnd $ wrap $ fsz 26

//let  dw = DebugWindow wnd

let pref = Pref("TwitDialog", wnd.Closed.wait)
let _wait      = pref.shF "読み上げの間隔" 0.
let _tweetwait = pref.shF "ツイート読み上げの間隔" 0.

let tokens = None'<Tokens>

let timer10ms = mkTimer 10

let prevCommand = None'<s>

[<RequireQualifiedAccess>]
type MessageSpeaking = Play | Pause | Engage
let messageSpeaking = sh MessageSpeaking.Play


let mkSpeech culture = 
  let __ = Speech'(culture, wnd.Closed.wait) 
  try' { pref.[sf "voice-%s" culture] |%| __.selVoice }
  __

let english  = mkSpeech "en-US"
let japanese = mkSpeech "ja-JP"

let iterSpeeches f =
  f english
  f japanese

