﻿[<AutoOpen>]
module __.SpeechQueue
open FStap open __ open System
open CoreTweet open CoreTweet.Streaming
open System.Speech.Synthesis


type SpeechBase = interface end 

type SpeechesBase<'a>(items:'a ra, _i:i, name:s) =
  let mutable _i = _i
  member __.next = 
    let b = _i + 1 < items.len 
    if b then &_i += 1
    b
  member __.prev =
    let b = _i - 1 >= 0
    if b then &_i -= 1
    b
  member __.current = items.[_i]
  member __.index with get () = _i and set i = _i <- i
  member __.name = name

type OperationBase = 
  inherit SpeechBase

type Operation(text:s) =
  member __.text = text
  interface OperationBase

type ResponseBase =
  inherit SpeechBase

type Response(text:s) =
  member __.text = text
  interface ResponseBase

type Selector<'a> (items:'a ra, name) =
  inherit SpeechesBase<'a>(items, 0, name)
  interface ResponseBase

type HelpSelector  (helps)   = inherit Selector<Command>(helps, "コマンド")
type SelectSelector(selects) = inherit Selector<Command>(selects, "コマンド")

type CommandKindSelector (speak, name:s, kinds) = 
  inherit Selector<CommandKind>(kinds, "コマンドの種類")
  member __.speak (kind:CommandKind) :u = speak kind
  member __.name = name

type VoiceSelector(voices, speech:Speech') =
  inherit Selector<VoiceInfo>(voices, "読み上げ音声")
  member __.speech = speech

type ListSelector(lists:List ra, speakList:CoreTweet.List -> u) = 
  inherit Selector<List>(lists, "リスト")
  member __.speakList list = speakList list

type UserSelector(users:User ra)      = inherit Selector<User>     (users, "ユーザー")
type UrlSelector (urls :UrlEntity ra) = inherit Selector<UrlEntity>(urls, "ゆーあーるえる")


type MessageBase = 
  inherit SpeechBase

type Message = 
  { text:s }
  interface MessageBase

type MessagesBase<'a>(items:'a ra, i, name:s, toText:'a -> s, onSpeak:'a -> u) =
  inherit SpeechesBase<'a>(items, i, name)
  interface MessageBase
  member __.toText = toText
  member __.onSpeak = onSpeak

type Messages<'a>(items:'a ra, i, name:s, toText:'a -> s, onSpeak:'a -> u, onFinished:u -> u) =
  inherit MessagesBase<'a>(items, i, name, toText, onSpeak)
  member __.onFinished = onFinished()

type Tweets(tweets, i, toText, onSpeak, onFinished) = 
  inherit Messages<Status>(tweets, i, "ツイート", toText, onSpeak, onFinished)

type DMs(dms, i, toText, onSpeak, onFinished) =
  inherit Messages<DirectMessage>(dms, i, "ダイレクトメッセージ", toText, onSpeak, onFinished)

type Stream(items:StreamingMessage ra, toText, onSpeak, received:StreamingMessage Event, disposer:IDisposable) =
  inherit MessagesBase<StreamingMessage>(items, 0, "新着", toText, onSpeak)
  member __.count = items.Count
  member __.received = received
  member __.disposer = disposer
  member __.nextForce =
    __.index <- __.index + 1
    __.index < __.count


type SpeechQueue<'a when 'a :> SpeechBase>() as __ =
  inherit Queue<'a>()
  let _cancel = Event<u>()
  do _cancel =+ { __.Clear() }
  member val cancel    = _cancel
  member val interrupt = Event<u>()
  member val restart   = Event<u>()
  member val isPlaying = true with get,set


let qOperations = SpeechQueue<OperationBase>()
let qResponses  = SpeechQueue<ResponseBase>()
let qMessages   = SpeechQueue<MessageBase>()

let enq  text  = Response text |> qResponses.enq
let enqs texts = texts |> Seq.iter enq
let inline enqf fmt = kf enq fmt

let enqOperation text = Operation text |> qOperations.enq
let enqMessage text = { Message.text = text } |> qMessages.enq

