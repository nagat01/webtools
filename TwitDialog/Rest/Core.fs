﻿[<AutoOpen>]
module Rest.Core
open FStap open __
open CoreTweet
open Client


let countQuery count (__:Params) =
  __.Add("count", count)
  __

let enqTweetsRepeat (api:_ -> #seq<Status>) toText fText textNone =
  let tweetsAll = ra()
  let max_id = None'<i64>
  let rec enqTweets n () =
    if n < 160 then
      let tweets = 
        match max_id with
        | Some' __ -> ["max_id", __]
        | _ -> []
        |> mkParams |> api
      let len = tweets.len
      if len > 0 then 
        if n = 0 then 
          qMessages.cancel <*- ()
        let i = tweetsAll.len
        tweetsAll.AddRange tweets
        fText len |> enqMessage
        Tweets(
          tweetsAll, 
          i,
          toText,
          (fun status -> 
            max_id <*- status.Id - 1L
            enqHistory status), 
          enqTweets (n+1) )
        |> qMessages.enq
      else
        enq textNone
  enqTweets 0 ()

let enqTweets       api fText textNone = enqTweetsRepeat api (fun __ -> __.text) fText textNone
let enqTweetsNoname api fText textNone = enqTweetsRepeat api (fun __ -> __.textNoname) fText textNone

let enqTweetsOnce fText textNone onSpeak onFinished (__:Status seq) =
  let len = __.len
  if len > 0 then
    qMessages.cancel <*- ()
    fText len |> enqMessage
    Tweets(ra __, 0, (fun status -> status.text), onSpeak, onFinished)
    |> qMessages.enq
  else
    enq textNone

