﻿[<AutoOpen>]
module Rest.Inbox
open FStap open __
open CoreTweet
open Client

let sinceIdDM     = pref.shI64 "sinceIdDM" 0L
let sinceIdReply  = pref.shI64 "sinceIdReply" 0L
let sinceIdDMSent = pref.shI64 "sinceIdDMSent" 0L

let enqDMs isStop fText testNone onSpeak onFinished (__:DirectMessage seq) =
  let len = __.len
  if len > 0 then
    if isStop then
      qMessages.cancel <*- ()
    fText len |> enqMessage
    DMs(ra __, 0, (fun dm -> dm.text), onSpeak, onFinished)
    |> qMessages.enq
  else
    enq testNone

let getDMs (tokens:Tokens) (q:Params) =
  let dm = tokens.DirectMessages
  query {
  for dm in Seq.append (dm.Received q) (dm.Sent q) do
  sortBy -dm.CreatedAt.Ticks }


let dm =
  Command(
    "dm", "でぃーえむ",
    "自分へのダイレクトメッセージと自分が送ったダイレクトメッセージを読み上げます。",
    @"dm と打つだけで使えます。"
, withTokens <| fun tokens _ -> 
  getDMs tokens (mkParams ["count", 100]) 
  |> enqDMs true
    (sf "ダイレクトメッセージが%d個取得出来ましたので読み上げます。") 
    "ダイレクトメッセージが見つかりませんでした。"
    (fun dm -> sinceIdDM <*- dm.Id )
    ig
  )

let speechNew (tokens:Tokens) (isReset:b) = 
  let query = mkParams ["count", 10]
  if sinceIdReply <>. 0L then 
    query.Add ("since_id", sinceIdReply.v)
  let replies = tokens.Statuses.MentionsTimeline(query).rev
  replies
  |> enqTweetsOnce (sf "新しい自分宛てのリプライが%d個あったので読み上げます。") "" 
    (fun status -> sinceIdReply <*- status.Id )
    ig
  let query = mkParams ["count", 20]
  if sinceIdDMSent <>. 0L then 
    query.Add ("since_id", sinceIdDMSent.v)
  let dms = ( getDMs tokens query |> Seq.truncate 10 ).rev
  dms
  |> enqDMs false (sf "新しいダイレクトメッセージが%d個あったので読み上げます。") ""
    (fun dm -> sinceIdDMSent <*- dm.Id )
    ig
  if isReset.n && replies.Length + dms.Length = 0 then
    enqf "新しい自分宛てのリプライとダイレクトメッセージが見つかりませんでした。"
    enq "new 空白 reset で、一度読んだものをリセットして再度読み上げます。"

let ``new`` =
  Command(
    "new", "にゅー",
    "新しい自分宛てのリプライとダイレクトメッセージを読み上げます。",
    @"new と入力すると、自分宛てのリプライ最新１０件と、ダイレクトメッセージ最新１０件を読み上げます。
    一度読んだものは再度読まないようになっていますが、new に空白をあけて reset で、一度読んだものをリセットして、再度読み上げます。。
    リセットすると、もう１回、新しい自分宛てのリプライとダイレクトメッセージを読み上げます。"
, withTokens <| fun tokens args ->
  match args with
  | String 1 ("reset" | "りせっと") -> 
    sinceIdReply <*- 0L
    sinceIdDMSent <*- 0L
    enq "一度読んだリプライとダイレクトメッセージをリセットしました。"
    speechNew tokens true
  | _ -> speechNew tokens false
  )

