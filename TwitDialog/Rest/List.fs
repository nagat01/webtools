﻿[<AutoOpen>]
module Rest.List
open FStap open __ open System
open CoreTweet
open Client
 
let speakList name (list:List) =
  tokens |%| fun tokens ->
    enqTweets
      (fun query -> 
        query.Add("list_id", list.Id)
        tokens.Lists.Statuses query)
      (sf "%sのリスト %s のツイートが%d個取得できましたので読み上げます" name list.Name)
      (sf "%sのリスト %s のツイートが見つかりませんでした" name list.Name)


let list =
  Command(
    "list","りすと",
    "リストのツイートを読み上げます",
    @"list で、自分が作成しているリストから、読み上げるリストを選択できます。
      list 空白 ユーザーID で、指定したユーザーのリストから、読み上げるリストを選択できます。
      読み上げられているリストを、うえ矢印キーで選択すると、選択したリストのツイートを読み上げることができます。
      list 空白 ユーザーID 空白 リストID で、指定したリストのツイートを読み上げます。"
  , withTokens <| fun tokens ->
  function
  | String 1 owner & String 2 list ->
    try
      enqTweets 
        (fun query -> 
          query.Add("owner_screen_name", owner)
          query.Add("slug", list)
          tokens.Lists.Statuses query )
        (sf "%sさんのリスト %s のツイートが%d個取得できましたので読み上げます" owner list)
        (sf "%sさんのリスト %s のツイートが見つかりませんでした" owner list)
    with _ -> enqf "%sさんのリスト %s を指定されましたが、そのようなリストは見つかりませんでした。" owner list
  | String 1 ("random" | "らんだむ") ->
    try
      let list = tokens.Lists.List().array.rand
      speakList "自分" list
    with _ -> enq "自分のリストが見つかりませんでした。"
  | String 1 name ->
    try
      let user = tokens.Users.Show(screen_name = name, include_entities = Nullable false)
      try enqListSelector (user.Name + "さん") (tokens.Lists.List(screen_name = name)) speakList 
      with _ -> enqf "%sさんのリストは見つかりませんでした。" name
    with _ -> userNotFound name
  | _ ->
    try enqListSelector "自分" (tokens.Lists.List()) speakList 
    with _ -> enq "自分のリストが見つかりませんでした。"
  )
