﻿[<AutoOpen>]
module Rest.Replies
open FStap open __
open CoreTweet
open Client

let cacheStatus = Dictionary<i64,Status>()


let track =
  Command(
    "track", "とらっく",
    "リプライさきを辿ってツイートを取得できます。",
    "track で、読み上げられた最新のツイートのリプライさきを辿って、5件までツイートを取得できます。"
, withTokens <| fun tokens _ ->
  match curTweet with
  | Some' status ->
    let mutable id = status.InReplyToStatusId
    if id.HasValue then
      let statuses = ResizeArray()
      let mutable n = 5
      let mutable next = true
      while (&n -= 1; n) >= 0 && next do
        let status = 
          match cacheStatus.find id.v with
          | Some status -> status
          | _ -> 
            let status = tokens.Statuses.Show id.v :> Status
            cacheStatus.[id.v] <- status
            status
        statuses <*- status
        id <- status.InReplyToStatusId
        next <- id.HasValue 
      qMessages.cancel <*- ()
      sf "リプライさきを辿ったツイートが %d個取得できましたので読み上げます。" statuses.len
      |> enqMessage
      Tweets(
        ra statuses,
        0, 
        (fun status -> status.text),
        (fun status -> enqHistory status), 
        ig)
      |> qMessages.enq 
    else
      enq "読み上げられた最新のツイートに含まれるリプライさきは見つかりませんでした。"
  | _ ->
    enq "読み上げられた最新のツイートはありませんでした。"
  )


let talk =
  Command(
    "talk", "とーく",
    "複数のユーザーの間でのリプライを読み上げられます。", 
    "talk に空白をあけて 複数のユーザーIDを空白区切りで書くと、指定した複数のユーザーの間でのリプライを読み上げます。"
  ).withUsage <| fun usage -> withTokens <| fun tokens args ->
  let users = Seq.tail args
  if users.len > 0 then
    let f (cmd:s->s) = ( users |%> cmd ).concat " OR "
    let keyword = sf "(%s) (%s)" (f (sf "from:%s")) (f (sf "to:%s")) 
    let users = 
      [ for user in tokens.Users.Lookup users -> sf "%sさん" user.Name ]
        .concat "と、"
    enqTweets
      (fun query -> 
        query.Add("q", keyword)
        query |> countQuery 50 |> tokens.Search.Tweets )
      (sf "%s のリプライが %d 個取得できましたので読み上げます" users)
      (sf "%s のリプライが見つかりませんでした" users)
  else 
    enq "ユーザーIDが指定されていません。"
    enq usage

