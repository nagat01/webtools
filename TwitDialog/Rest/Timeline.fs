﻿[<AutoOpen>]
module Rest.Timeline
open FStap open __
open Client


let home = 
  Command(
    "home", "ほーむ",
    "ホームタイムラインを読み上げます。",
    "home と打つだけで使えます。"
, withTokens <| fun tokens _ ->
  enqTweets 
    (fun query -> query |> countQuery 200 |> tokens.Statuses.HomeTimeline)
    (sf "ホームタイムラインのツイートが%i個取得出来ましたので読み上げます")
    "ホームタイムラインのツイートが見つかりませんでした"
  )


let mention =
  Command(
    "mention", "めんしょん",
    "自分へのリプライを読み上げます。",
    "mention と打つだけで使えます。"
, withTokens <| fun tokens _ ->
  enqTweets
    (fun query -> query |> countQuery 200 |> tokens.Statuses.MentionsTimeline)
    (sf "自分へのリプライが%i個取得出来ましたので読み上げます")
    "自分へのリプライが見つかりませんでした" 
  )


let user =
  Command(
    "user","ゆーざー",
    "指定したユーザーのツイートを読み上げます。",
    @"userと入力するだけで、自分のツイートを読み上げます。
      user に空白をあけて ユーザーID で、指定したユーザーのツイートを読み上げます。"
, withTokens <| fun tokens args ->
  let user = match args with | String 1 user -> user | _ -> ""
  try 
    let name = 
      if user = "" then "自分" 
      else tokens.Users.Show(screen_name = user).Name + "さん"
    enqTweetsNoname 
      (fun query ->
        if user <> "" then query.Add ("screen_name", user)
        query |> countQuery 50 |> tokens.Statuses.UserTimeline)
      (sf "%sのツイートが%i個取得できましたので読み上げます" name)
      (sf "%sのツイートが見つかりませんでした" name)
  with _ -> userNotFound user
  )


let favorite =
  Command(
    "favorite", "ふぇいばりっと",
    "ふぁぼを読み上げます。",
    @"favoriteと入力するだけで、自分のふぁぼを読み上げます。
    favorite に空白をあけて ユーザーID で、指定したユーザーのふぁぼを読み上げます。"
, withTokens <| fun token args ->
  let user = match args with | String 1 user -> user | _ -> ""
  try
    let name = if user = "" then "自分" else token.Users.Show(screen_name = user).Name + "さん"
    enqTweets 
      (fun query ->
        if user <> "" then query.Add("screen_name", user)
        query |> countQuery 50 |> token.Favorites.List )
      (sf "%sのふぁぼが%d個取得できましたので読み上げます。" name)
      (sf "%sのふぁぼが見つかりませんでした。" name)
  with _ -> userNotFound user
  )


let search = 
  Command(
    "search", "さーち",
    "Twitter公式の検索コマンドに一致するツイートを読み上げます。",
    @"search に空白をあけて 検索コマンド で、指定した検索コマンドに一致するツイートを読み上げます。
      検索コマンドには、キーワードやハッシュタグを指定できます。
      検索コマンドは空白区切りで複数書くことができます。"
  ).withUsage <| fun usage -> withTokens <| fun tokens args ->
  let q = (Seq.tail args).concat " "
  if q.len > 0 then
    enqTweets
      (fun query -> 
        query.Add("q", q)
        query |> countQuery 50 |> tokens.Search.Tweets)
      (sf "検索コマンド%sのツイートが%d個取得できましたので読み上げます" q)
      (sf "検索コマンド%sのツイートが見つかりませんでした" q)
  else
    enq "検索コマンドが入力されていません。"
    enq usage

