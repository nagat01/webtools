﻿module Speech
open FStap open __

let _volume = pref.shI "読み上げボリューム" 50
let _speed  = pref.shI "読み上げの速度" 0

let inline normalize mi ma x textLower testUpper testSet =
  if   x < mi then enqf textLower x
  elif x > ma then enqf testUpper x
  let x = within mi ma x
  enqf testSet x
  x


let volume = 
  Command(
    "volume","ぼりゅーむ",
    "読み上げボリュームを変更できます。", 
    "volumeに空白を開けて 10 から 100 までの数値を指定すると、読み上げボリュームを設定できます。", function
  | Int 1 volume ->
    normalize 10 100 volume
      "volume %d と指定されましたが、読み上げのボリュームは 10 以上を指定してください"
      "volume %d と指定されましたが、読み上げのボリュームは 100以下を指定してください"
      "読み上げボリュームを %i に設定しました"
    |>* _volume 
  | _ -> 
    enq "読み上げボリュームを変更する場合、 volume と書いたあとに空白をおき 10 から 100 までの数値を指定してください"
  )


let speed =
  Command(
    "speed","すぴーど", 
    "読み上げスピードを変更できます。",
    "speedに空白を開けて -10 から 10 までの数値で指定すると、読み上げスピードを設定できます。", function
  | Int 1 speed ->
    normalize -10 10 speed
      "speed %d と指定されましたが、読み上げスピードは -10以上を指定してください"
      "speed %d と指定されましたが、読み上げスピードは  10以下を指定してください"
      "読み上げスピードを %i に設定しました"
    |>* _speed 
  | _ ->
    enq "読み上げスピードを変更する場合、speed と書いたあとに空白をおき -10 から 10 までの数値を指定してください"
  )


let wait =
  Command(
    "wait","うぇいと", 
    "読み上げの間隔を変更できます。",
    "waitに空白をあけて 0.0 から 2.0 までの数字で秒数を指定すると、読み上げの間隔を設定できます。", function
  | Float 1 wait ->
    normalize 0. 2. wait
      "wait %.1f と指定されましたが、読み上げの間隔は秒数で 0.0 以上を指定してください" 
      "wait %.1f と指定されましたが、読み上げの間隔は秒数で 2.0 以下を指定してください" 
      "読み上げの間隔を %.1f 秒に設定しました"
    |>* _wait 
  | _ ->
    enq "読み上げの間隔を変更する場合、 wait と書いたあとに空白をおいて、0.0 から 2.0 までの数字で秒数を指定してください"
  )


let tweetwait =
  Command(
    "tweetwait","ついーとうぇいと", 
    "ツイート読み上げの間隔を変更できます。",
    "tweetwaitに空白をあけて 0.0 から 10.0 までの数字で秒数を指定すると、ツイート読み上げの間隔を設定できます。", function
  | Float 1 wait ->
    normalize 0. 10. wait
      "tweetwait %.1f と指定されましたが、ツイート読み上げの間隔は秒数で 0.0 以上を指定してください" 
      "tweetwait %.1f と指定されましたが、ツイート読み上げの間隔は秒数で 10.0 以下を指定してください" 
      "ツイート読み上げの間隔を %.1f 秒に設定しました"
    |>* _tweetwait 
  | _ ->
    enq "ツイート読み上げの間隔を変更する場合、 tweetwait と書いたあとに空白をおいて、0.0 から 10.0 までの数字で秒数を指定してください"
  )


let voice =
  Command(
    "voice","ぼいす",
    "読み上げの音声を変更できます。",
    @"voice で、読み上げの音声が順番に読み上げられます。
      voice に空白をあけて english で、英語音声の読み上げを変更できます。
      うえ矢印キーで、読み上げ中の音声を、読み上げの音声に指定できます。
      した矢印キーで、今の音声をもう１回読み上げます。
      ひだり矢印キーで前の音声、みぎ矢印キーで次の音声を指定できます。", fun args ->
  let speech, lang =
    match args with
    | String 1 ("english" | "いんぐりっしゅ") -> english, "英語"
    | _ -> japanese, "日本語"
  enqf "%sの読み上げの音声を変更できます。読み上げられている音声に切り替えたい場合は、うえ矢印キーを押してください。" lang
  VoiceSelector(ra speech.voices, speech)
  |> qResponses.enq
  )


let ini =
  _volume => fun volume -> iterSpeeches (fun __ -> __.volume <- volume)
  _speed  => fun speed  -> iterSpeeches (fun __ -> __.speed  <- speed)

