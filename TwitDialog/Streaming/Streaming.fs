﻿module Streaming
open FStap open __
open System
open CoreTweet
open CoreTweet.Streaming
open Client


type StatusMessage with
  member __.text = 
    let __ = __.Status
    sf "%sさんの新着、%s、%s、%s" __.User.Name __.text __.fav __.rt  

type DirectMessageMessage with
  member __.text =
    let __ = __.DirectMessage
    sf "%sさんからダイレクトメッセージが届きました。%s前、%s" 
      __.Sender.Name __.elapsed __.Text


let enqStream (__:StreamingMessage obs) =
  let items = ra()
  let received = Event<_>()
  Stream(
    items, 
    (function
    | :? StatusMessage as __ -> __.text
    | :? DirectMessageMessage as __ -> __.text
    | :? EventMessage as __ -> sf "イベントメッセージ %A" __.Event
    | message -> sf "メッセージ %A" (message.Type.ToString())),
    (function
    | :? StatusMessage as __ -> enqHistory __.Status
    | :? DirectMessageMessage as __ -> ()
    | _ -> ()),
    received,
    __.Subscribe (fun __ ->
      match __ with
      | :? StatusMessage | :? DirectMessageMessage ->
        items.Add __
        received <*- __
      | _ -> ())
  ) |> qMessages.enq


let stream =
  Command(
    "stream", "すとりーむ",
    "新着ツイートを読み上げます。", 
    @"stream のみだとホームタイムラインを読み上げます。
    stream に空白をあけてキーワードを入力すると、そのキーワードの新着ツイートを読み上げます。
    キーワードは空白区切りで複数書くことができます。"
, withTokens <| fun tokens args ->
  let keywords = (Seq.tail args).concat ","
  qMessages.cancel <*- ()
  if keywords.len > 0 then
    enqf "stream でキーワード %s の新着の読み上げを開始します。" keywords
    tokens.Streaming.FilterAsObservable(track = keywords) 
    |> enqStream 
  else
    enq "stream でホームタイムラインの新着の読み上げを開始します。"
    tokens.Streaming.UserAsObservable() 
    |> enqStream
  )


let enqListStream (tokens:Tokens) name (list:List) =
  let members =
    tokens.Lists.Members.List(
      list_id = list.Id, 
      count = Nullable list.MemberCount,
      include_entities = Nullable false)
  let ids = seq {
    for user in members do
      if user.Id.HasValue then
        yield user.Id.Value }
  qMessages.cancel <*- ()
  enqf "%s のリスト %s の新着を読み上げます。" name list.Name
  tokens.Streaming.FilterAsObservable(follow = ids)
  |> enqStream

let liststream =
  Command(
    "liststream", "りすとすとりーむ",
    "リストの新着ツイートを読み上げます。", 
    @"liststream で、自分が作成しているリストから、新着を読み上げるリストを選択できます。
      liststream 空白 ユーザーID で、指定したユーザーのリストから、新着を読み上げるリストを選択できます。
      読み上げられているリストを、うえ矢印キーで選択すると、選択したリストの新着を読み上げることができます。
      liststream 空白 ユーザーID 空白 リストID で、指定したリストの新着を読み上げます。"
  , withTokens <| fun tokens -> 
  function
  | String 1 owner & String 2 list ->
    try
      tokens.Lists.Show(owner_screen_name = owner, slug = list)
      |> enqListStream tokens (owner + "さん")
    with _ -> enqf "%s さんのリスト %s を指定されましたが、そのようなリストは見つかりませんでした。" owner list
  | String 1 ("random" | "らんだむ") ->
    try
      tokens.Lists.List().array.rand
      |> enqListStream tokens "自分"
    with _ -> enq "ランダムに選んだ自分のリストの読み上げに失敗しました。"
  | String 1 name ->
    try
      let user = tokens.Users.Show(screen_name = name, include_entities = Nullable false)
      try
        enqListSelector 
          (user.Name + "さん") 
          (tokens.Lists.List(screen_name = name))
          (enqListStream tokens)
      with _ -> enqf "%sさんのリストの新着の読み上げに失敗しました。" name
    with _ -> enqf "ユーザーID %s と指定されましたが、そのようなユーザーは見つかりませんでした。" name
  | _ ->
    try enqListSelector "自分" (tokens.Lists.List()) (enqListStream tokens)
    with _ -> enq "自分のリストが見つかりませんでした。"
  )

